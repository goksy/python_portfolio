class Person():
    # constructor
    def __init__(self, firstname, lastname, street, city, state, postcode):
        self.firstname = firstname
        self.lastname = lastname
        self.street = street
        self.city = city
        self.state = state
        self.postcode = postcode

    # method
    def getFirstName(self):
        return self.firstname

    # method
    def getLastName(self):
        return self.lastname

    # method
    def getStreet(self):
        return self.street

    # method
    def getCity(self):
        return self.city

    # method
    def getState(self):
        return self.state

    # method
    def getPostcode(self):
        return self.postcode

    def __str__(self):
        return '{},{},{},{},{},{}'.format(self.firstname, self.lastname, self.street, self.city, self.state, self.postcode)
