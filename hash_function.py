from person import Person

fileref = open('smallData.csv', 'r')  # r is for read
#fileref = open('mediumData.dat', 'r')  # r is for read
#fileref = open('largeData.dat', 'r')  # r is for read

lines = fileref.readlines()

firstname = ''
lastname = ''
street = ''
city = ''
state = ''
postcode = ''
people = []

for line in lines:
    firstname, lastname, street, city, state, postcode = line.strip().split(',')
    person = Person(firstname, lastname, street, city, state, postcode)
    people.append(person)

# for person in people:
#     print(person)
people_dict = {}
for person in people:
    people_dict[hash(person)] = person
    # print key: value
for k, v in people_dict.items():
    print(k, ':', v)
