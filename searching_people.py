from person import Person
import time
import math

fileref = open('smallData.csv', 'r')  # r is for read
#fileref = open('mediumData.dat', 'r')  # r is for read
#fileref = open('largeData.dat', 'r')  # r is for read

lines = fileref.readlines()

firstname = ''
lastname = ''
street = ''
city = ''
state = ''
postcode = ''
people = []

for line in lines:
    firstname, lastname, street, city, state, postcode = line.strip().split(',')
    person = Person(firstname, lastname, street, city, state, postcode)
    people.append(person)

#for person in people:
 #   print(person)

#############################
#linear Search
#data doesnt need to be sorted
def linearSearch(people, searchString):
    for index in range (len(people)):
        if people[index].firstname == searchString:
            return print(people[index])
    return -1
###############################
#Binary search
#data must be sorted

def binarySearch(people, searchString):
    first = 0
    last = len(people)-1
    index = -1
    while (first <= last) and (index == -1):
        mid = (first+last)//2
        if people[mid].firstname == searchString:
            index = mid
        else:
            if searchString < people[mid].firstname:
                last = mid -1
            else:
                first = mid +1
    return print(people[index])
###############################
# Jump search
# data must be sorted
def jumpSearch(people, searchString):
    length = len(people)
    jump = int(math.sqrt(length))
    left, right = 0, 0
    while left < length and people[left].firstname <= searchString:
        right = min(length - 1, left + jump)
        if people[left].firstname <= searchString and people[right].firstname >= searchString:
            break
        left += jump
    if left >= length or people[left].firstname > searchString:
        return -1
    right = min(length - 1, right)
    index = left
    while index <= right and people[index].firstname <= searchString:
        if people[index].firstname == searchString:
            return print(people[index])
        index += 1
    return -1
############################
searchString = 'Sara'
############################
start = time.perf_counter()
linearSearch(people, searchString)
end = time.perf_counter()
linearSearchTime = end - start
############################
start = time.perf_counter()
binarySearch(people, searchString)
end = time.perf_counter()
binarySearchTime = end - start
#############################
start = time.perf_counter()
jumpSearch(people, searchString)
end = time.perf_counter()
jumpSearchTime = end - start
#############################

print('\n Linear Search: ' + str(linearSearchTime))
print('\n Binary Search: ' + str(binarySearchTime))
print('\n Jump Search: ' + str(jumpSearchTime))
