from person import Person
import time

import sys
print(sys.getrecursionlimit())
sys.setrecursionlimit(5000)
print(sys.getrecursionlimit())
fileref = open('smallData.dat', 'r')  # r is for read
#fileref = open('mediumData.dat', 'r')  # r is for read
#fileref = open('largeData.dat', 'r')  # r is for read

lines = fileref.readlines()

firstname = ''
lastname = ''
street = ''
city = ''
state = ''
postcode = ''
people = []

for line in lines:
    if ',' in line:
        lastname, firstname = line.strip().split(',')
        continue
    elif line[0].isdigit():
        street = line.strip()
        continue
    elif len(line) > 1:
        city, state, postcode = line.strip().split('\t')
        continue

    person = Person(firstname, lastname, street, city, state, postcode)
    people.append(person)

#print('\nUnsorted People: ')
#for person in people:
#print(person)

# Bubble Sort
def bubbleSort(people):
    n = len(people)

    # Traverse through all array elements
    for i in range(n):
        # Last i elements are already in place
        for j in range(0, n-i-1):
            # traverse the array from 0 to n-i-1
            # Swap if the element found is greater
            # than the next element
            if people[j].firstname > people[j+1].firstname :
                people[j], people[j+1] = people[j+1], people[j]

#Merge sort
def mergeSort(people):
    if len(people) > 1:
        mid = len(people)//2 # Finding the mid of the array
        L = people[:mid] # Dividing the array elements
        R = people[mid:] # into 2 halves

        mergeSort(L) # Sorting the first half
        mergeSort(R) # Sorting the second half

        i = j = k = 0
        # Copy data to temp arrays L[] and R[]
        while i < len(L) and j < len(R):
            if L[i].firstname < R[j].firstname:
                people[k] = L[i]
                i+= 1
            else:
                people[k] = R[j]
                j+= 1
            k+= 1
             # Checking if any element was left
        while i < len(L):
            people[k] = L[i]
            i+= 1
            k+= 1

        while j < len(R):
            people[k] = R[j]
            j+= 1
            k+= 1

#quick sort
# This function takes last element as pivot, places
# the pivot element at its correct position in sorted
# array, and places all smaller (smaller than pivot)
# to left of pivot and all greater elements to right
# of pivot
def partition(people,low,high):
    i = (low-1)         # index of smaller element
    pivot = people[high].firstname     # pivot

    for j in range(low, high):

        # If current element is smaller than the pivot
        if people[j].firstname < pivot:
            # increment index of smaller element
            i = i+1
            people[i], people[j] = people[j], people[i]

    people[i+1], people[high] = people[high], people[i+1]
    return i+1
# The main function that implements QuickSort
# arr[] --> Array to be sorted,
# low  --> Starting index,
# high  --> Ending index
low = 0
high = len(people) - 1
# Function to do Quick sort
def quickSort(people, low, high):
    if low < high:

    # pi is partitioning index, arr[p] is now
    # at right place
        pi = partition(people, low, high)

    # Separately sort elements before
    # partition and after partition
        quickSort(people, low, pi-1)
        quickSort(people, pi+1, high)

# python Sort
def pythonSort(people):
    people.sort(key=lambda person: person.firstname.upper(), reverse=False)
#####################################
start = time.perf_counter()
bubbleSort(people)
end = time.perf_counter()
bubbleSortTime = end - start
print('Bubble Sort Time: ' + str(bubbleSortTime))

# print('\n Bubble Sorted People: ')
# for person in people:
#     print(person)

#####################################
print('\n')
start = time.perf_counter()
mergeSort(people)
end = time.perf_counter()
mergeSortTime = end - start
print('Merge Sort Time: ' + str(mergeSortTime))

# print('\n Merge Sorted People: ')
# for person in people:
#     print(person)

#####################################
print('\n')
start = time.perf_counter()

quickSort(people, low, high)
end = time.perf_counter()
quickSortTime = end - start
print('Quick Sort Time: ' + str(quickSortTime))

# print('\n Quick Sorted People: ')
# for person in people:
#     print(person)

#####################################
print('\n')
start = time.perf_counter()
pythonSort(people)
end = time.perf_counter()
pythonSortTime = end - start
print('Python Sort Time: ' + str(pythonSortTime))

# print('\n Python Sorted People: ')
# for person in people:
#     print(person)

#####################################

# outfile = open('mediummData.csv.csv', 'w')
# for person in people:
#     outfile.write(str(person) + '\n')
# outfile.close()
