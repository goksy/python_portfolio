from person import Person

fileref = open('smallData.csv', 'r')  # r is for read
#fileref = open('mediumdata.csv', 'r')  # r is for read
#fileref = open('largedata.csv', 'r')  # r is for read
lines = fileref.readlines()

firstname = ''
lastname = ''
street = ''
city = ''
state = ''
postcode = ''
people = []

for line in lines:
    firstname, lastname, street, city, state, postcode = line.strip().split(',')
    person = Person(firstname, lastname, street, city, state, postcode)
    people.append(person)

    # for data in people:
    #     print(data)
#############################
    class Node:

        def __init__(self, data):

            self.left = None
            self.right = None
            self.data = data

        def insert(self, data):

            if self.data:
                if data.firstname < self.data.firstname:
                    if self.left is None:
                        self.left = Node(data)
                    else:
                        self.left.insert(data)
                elif data.firstname > self.data.firstname:
                    if self.right is None:
                        self.right = Node(data)
                    else:
                        self.right.insert(data)
            else:
                self.data = data

        def PrintTree(self):
            if self.left:
                self.left.PrintTree()
            print(self.data),
            if self.right:
                self.right.PrintTree()

# Use the insert method to add nodes
root = Node(people[0])
for person in people[1:]:
    root.insert(person)

root.insert(Person('Gokhan', 'Onder', '35 Plenty', 'Preston', 'Vic', '2222'))
root.insert(Person('George', 'Smith', '35 Brunswick Rd', 'Brunswick', 'Vic', '3322'))
root.PrintTree()
